#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void readdatatriptime(char **filename, double *result)
{
    char buffer[1024];
    char *line;
    int i, n = 0;
    char *tmp = NULL;
    
    FILE *fstream = fopen(*filename,"r");
    
    if(fstream == NULL)
    {
        printf("\n file opening failed ");
        return;
    }
    
    fgets(buffer,sizeof(buffer),fstream);
    
    while((line=fgets(buffer,sizeof(buffer),fstream))!=NULL)
    {
        
        for(i = 0; i<9; i++){
            tmp =  strsep(&line,",");
        }
        
        result[n] =atof(tmp);
        n = n + 1;
    }
    // make sure we close the file when we're finished
    fclose(fstream);
    
}