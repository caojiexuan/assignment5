#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void check(char **filename1, char **filename2, int *result)
{
    char buffer1[1024], buffer2[1024];;
    char *line1, *line2;
    char *tmp1 = NULL, *tmp2 = NULL;
    
    FILE *fstream1 = fopen(*filename1,"r");
    FILE *fstream2 = fopen(*filename2,"r");
    
    if(fstream1 == NULL||fstream2 == NULL)
    {
        printf("\n file opening failed ");
        return;
    }
    
    
    fgets(buffer1,sizeof(buffer1),fstream1);
    fgets(buffer2,sizeof(buffer2),fstream2);
    
    
    while((line1=fgets(buffer1,sizeof(buffer2),fstream1))!=NULL  && (line2=fgets(buffer2,sizeof(buffer2),fstream2))!=NULL)
    {
        
        for(int i = 0; i<3; i++){
            
            tmp1 =  strsep(&line1,",");
            tmp2 =  strsep(&line2,",");
            
            if (strcmp (tmp1, tmp2) != 0){
                *result = 1;
                return;
            }
            
            
        }
    }

    // make sure we close the file when we're finished
    fclose(fstream1);
    fclose(fstream2);
    
    
}
