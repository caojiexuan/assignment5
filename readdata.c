#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void readdata(char **filename, double *result, double *surcharge)
{
    char buffer[1024];
    char *line;
    int i, n = 0;
    double total_amount = 0;
    double tolls_amount = 0;
    char *tmp = NULL;
    
    FILE *fstream = fopen(*filename,"r");
    
    if(fstream == NULL)
    {
        printf("\n file opening failed ");
        return;
    }
    
    fgets(buffer,sizeof(buffer),fstream);
    
    while((line=fgets(buffer,sizeof(buffer),fstream))!=NULL)
    {
        
        for(i = 0; i<7; i++){
            tmp =  strsep(&line,",");
        }
        surcharge[n] = atof(tmp);
        
        for(i = 0; i<3; i++){
            tmp =  strsep(&line,",");
        }
        
        tolls_amount = atof(tmp);
        
        tmp = strsep(&line, ",");
        total_amount = atof(tmp);
        
        result[n] = total_amount - tolls_amount;
        
        n = n + 1;
    }
    // make sure we close the file when we're finished
    fclose(fstream);
    
}