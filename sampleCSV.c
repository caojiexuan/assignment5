
#include <stdio.h>
#include <stdlib.h>


/*
 This version isn't as flexible as it might be.
 We allow the caller to specify the row numbers to sample 
 on the command line or on standard input.
 We could allow them to specify a file that contains these
 row numbers. But we haven't implemented that here to keep things simple.
 Also, we assume that the row numbers are ordered. We could sort them
 ourselves, but there is no need.

 We also could allow the caller to specify multiple parallel files
 and then the row numbers. We'd need an identifier to separate
 the set of file names and the row numbers.
*/

int readNLines(int jump, char *buf, FILE *f);
int nextLineNumber(int curLine, int argc, char *argv[]);

#define MAX_LINE_LEN 1000

int
main(int argc, char *argv[])
{
    if(argc < 2) {
	printf("sampleCSV  file  rowNum rowNum rowNum ...\n");
	exit(1);
    }

    FILE *f;
    if(! (f = fopen(argv[1], "r")) ) {
	fprintf(stderr, "cannot open file '%s'\n", argv[1]);
	exit(2);
    }

    int lineNum;
    int curLineNum = 0;
    char buf[MAX_LINE_LEN + 1];
    int numProcessed = 0;

    while( (lineNum = nextLineNumber(numProcessed, argc, argv)) > 0 ) {
      if(lineNum < curLineNum) {
	fprintf(stderr, "row identifiers are not sorted. Terminating\n");
	exit(3);
      }

      if(readNLines(lineNum  - curLineNum, buf, f))
	printf("%s", buf);
      else {
	fprintf(stderr, "incorrect line number. Too large\n");
	break;
      }


      numProcessed++;
      curLineNum = lineNum;
    }

    fclose(f);
    exit(0); // success.
}


int
readNLines(int jump, char *buf, FILE *f)
{
    char *status = buf;
    int i;
    for(i = 0; i < jump && status ; i++) 
	status = fgets(buf, MAX_LINE_LEN, f);

    return(status != NULL);
}

int
nextLineNumber(int curIndex, int argc, char *argv[])
{
    char tmp[10];
    if(argc == 2) {
	fgets(tmp, sizeof(tmp)/sizeof(tmp[0]), stdin);
	return(atoi(tmp));
    } else {
      if(curIndex + 2 >= argc)
	return(-1);
      return(atoi(argv[curIndex + 2]));
    }
}
